// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffBase.h"
#include "SnakeBase.h"

// Sets default values
ABuffBase::ABuffBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABuffBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABuffBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABuffBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->MovementSpeed = (MovementSpeed - 0.4f);
			Destroy();
		}
	}
}

