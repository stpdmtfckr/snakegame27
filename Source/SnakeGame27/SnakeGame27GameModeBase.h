// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGame27GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME27_API ASnakeGame27GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
