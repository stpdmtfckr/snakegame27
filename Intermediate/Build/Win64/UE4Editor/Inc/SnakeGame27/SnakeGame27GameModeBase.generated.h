// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME27_SnakeGame27GameModeBase_generated_h
#error "SnakeGame27GameModeBase.generated.h already included, missing '#pragma once' in SnakeGame27GameModeBase.h"
#endif
#define SNAKEGAME27_SnakeGame27GameModeBase_generated_h

#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_SPARSE_DATA
#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_RPC_WRAPPERS
#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeGame27GameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGame27GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGame27GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame27"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGame27GameModeBase)


#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeGame27GameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGame27GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGame27GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame27"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGame27GameModeBase)


#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGame27GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGame27GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGame27GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGame27GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGame27GameModeBase(ASnakeGame27GameModeBase&&); \
	NO_API ASnakeGame27GameModeBase(const ASnakeGame27GameModeBase&); \
public:


#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGame27GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGame27GameModeBase(ASnakeGame27GameModeBase&&); \
	NO_API ASnakeGame27GameModeBase(const ASnakeGame27GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGame27GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGame27GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGame27GameModeBase)


#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_12_PROLOG
#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_SPARSE_DATA \
	SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_RPC_WRAPPERS \
	SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_INCLASS \
	SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_SPARSE_DATA \
	SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME27_API UClass* StaticClass<class ASnakeGame27GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame27_Source_SnakeGame27_SnakeGame27GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
