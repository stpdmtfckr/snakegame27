// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame27/WallBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWallBase() {}
// Cross Module References
	SNAKEGAME27_API UClass* Z_Construct_UClass_AWallBase_NoRegister();
	SNAKEGAME27_API UClass* Z_Construct_UClass_AWallBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame27();
	SNAKEGAME27_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void AWallBase::StaticRegisterNativesAWallBase()
	{
	}
	UClass* Z_Construct_UClass_AWallBase_NoRegister()
	{
		return AWallBase::StaticClass();
	}
	struct Z_Construct_UClass_AWallBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWallBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame27,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WallBase.h" },
		{ "ModuleRelativePath", "WallBase.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AWallBase_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AWallBase, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWallBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWallBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWallBase_Statics::ClassParams = {
		&AWallBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWallBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWallBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWallBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWallBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWallBase, 4164525880);
	template<> SNAKEGAME27_API UClass* StaticClass<AWallBase>()
	{
		return AWallBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWallBase(Z_Construct_UClass_AWallBase, &AWallBase::StaticClass, TEXT("/Script/SnakeGame27"), TEXT("AWallBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWallBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
