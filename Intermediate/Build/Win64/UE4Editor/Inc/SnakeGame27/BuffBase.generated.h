// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME27_BuffBase_generated_h
#error "BuffBase.generated.h already included, missing '#pragma once' in BuffBase.h"
#endif
#define SNAKEGAME27_BuffBase_generated_h

#define SnakeGame27_Source_SnakeGame27_BuffBase_h_14_SPARSE_DATA
#define SnakeGame27_Source_SnakeGame27_BuffBase_h_14_RPC_WRAPPERS
#define SnakeGame27_Source_SnakeGame27_BuffBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame27_Source_SnakeGame27_BuffBase_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABuffBase(); \
	friend struct Z_Construct_UClass_ABuffBase_Statics; \
public: \
	DECLARE_CLASS(ABuffBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame27"), NO_API) \
	DECLARE_SERIALIZER(ABuffBase) \
	virtual UObject* _getUObject() const override { return const_cast<ABuffBase*>(this); }


#define SnakeGame27_Source_SnakeGame27_BuffBase_h_14_INCLASS \
private: \
	static void StaticRegisterNativesABuffBase(); \
	friend struct Z_Construct_UClass_ABuffBase_Statics; \
public: \
	DECLARE_CLASS(ABuffBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame27"), NO_API) \
	DECLARE_SERIALIZER(ABuffBase) \
	virtual UObject* _getUObject() const override { return const_cast<ABuffBase*>(this); }


#define SnakeGame27_Source_SnakeGame27_BuffBase_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABuffBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABuffBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuffBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuffBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuffBase(ABuffBase&&); \
	NO_API ABuffBase(const ABuffBase&); \
public:


#define SnakeGame27_Source_SnakeGame27_BuffBase_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuffBase(ABuffBase&&); \
	NO_API ABuffBase(const ABuffBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuffBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuffBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABuffBase)


#define SnakeGame27_Source_SnakeGame27_BuffBase_h_14_PRIVATE_PROPERTY_OFFSET
#define SnakeGame27_Source_SnakeGame27_BuffBase_h_11_PROLOG
#define SnakeGame27_Source_SnakeGame27_BuffBase_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame27_Source_SnakeGame27_BuffBase_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame27_Source_SnakeGame27_BuffBase_h_14_SPARSE_DATA \
	SnakeGame27_Source_SnakeGame27_BuffBase_h_14_RPC_WRAPPERS \
	SnakeGame27_Source_SnakeGame27_BuffBase_h_14_INCLASS \
	SnakeGame27_Source_SnakeGame27_BuffBase_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame27_Source_SnakeGame27_BuffBase_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame27_Source_SnakeGame27_BuffBase_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame27_Source_SnakeGame27_BuffBase_h_14_SPARSE_DATA \
	SnakeGame27_Source_SnakeGame27_BuffBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame27_Source_SnakeGame27_BuffBase_h_14_INCLASS_NO_PURE_DECLS \
	SnakeGame27_Source_SnakeGame27_BuffBase_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME27_API UClass* StaticClass<class ABuffBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame27_Source_SnakeGame27_BuffBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
