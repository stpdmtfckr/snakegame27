// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME27_WallBase_generated_h
#error "WallBase.generated.h already included, missing '#pragma once' in WallBase.h"
#endif
#define SNAKEGAME27_WallBase_generated_h

#define SnakeGame27_Source_SnakeGame27_WallBase_h_13_SPARSE_DATA
#define SnakeGame27_Source_SnakeGame27_WallBase_h_13_RPC_WRAPPERS
#define SnakeGame27_Source_SnakeGame27_WallBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame27_Source_SnakeGame27_WallBase_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWallBase(); \
	friend struct Z_Construct_UClass_AWallBase_Statics; \
public: \
	DECLARE_CLASS(AWallBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame27"), NO_API) \
	DECLARE_SERIALIZER(AWallBase) \
	virtual UObject* _getUObject() const override { return const_cast<AWallBase*>(this); }


#define SnakeGame27_Source_SnakeGame27_WallBase_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAWallBase(); \
	friend struct Z_Construct_UClass_AWallBase_Statics; \
public: \
	DECLARE_CLASS(AWallBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame27"), NO_API) \
	DECLARE_SERIALIZER(AWallBase) \
	virtual UObject* _getUObject() const override { return const_cast<AWallBase*>(this); }


#define SnakeGame27_Source_SnakeGame27_WallBase_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWallBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWallBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWallBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWallBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWallBase(AWallBase&&); \
	NO_API AWallBase(const AWallBase&); \
public:


#define SnakeGame27_Source_SnakeGame27_WallBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWallBase(AWallBase&&); \
	NO_API AWallBase(const AWallBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWallBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWallBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWallBase)


#define SnakeGame27_Source_SnakeGame27_WallBase_h_13_PRIVATE_PROPERTY_OFFSET
#define SnakeGame27_Source_SnakeGame27_WallBase_h_10_PROLOG
#define SnakeGame27_Source_SnakeGame27_WallBase_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame27_Source_SnakeGame27_WallBase_h_13_PRIVATE_PROPERTY_OFFSET \
	SnakeGame27_Source_SnakeGame27_WallBase_h_13_SPARSE_DATA \
	SnakeGame27_Source_SnakeGame27_WallBase_h_13_RPC_WRAPPERS \
	SnakeGame27_Source_SnakeGame27_WallBase_h_13_INCLASS \
	SnakeGame27_Source_SnakeGame27_WallBase_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame27_Source_SnakeGame27_WallBase_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame27_Source_SnakeGame27_WallBase_h_13_PRIVATE_PROPERTY_OFFSET \
	SnakeGame27_Source_SnakeGame27_WallBase_h_13_SPARSE_DATA \
	SnakeGame27_Source_SnakeGame27_WallBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame27_Source_SnakeGame27_WallBase_h_13_INCLASS_NO_PURE_DECLS \
	SnakeGame27_Source_SnakeGame27_WallBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME27_API UClass* StaticClass<class AWallBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame27_Source_SnakeGame27_WallBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
